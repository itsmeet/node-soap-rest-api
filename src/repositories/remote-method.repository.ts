import {DefaultCrudRepository} from '@loopback/repository';
import {RemoteMethod, RemoteMethodRelations} from '../models';
import {NodeSoapRestApiDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RemoteMethodRepository extends DefaultCrudRepository<
  RemoteMethod,
  typeof RemoteMethod.prototype.name,
  RemoteMethodRelations
> {
  constructor(
    @inject('datasources.node_soap_rest_api') dataSource: NodeSoapRestApiDataSource,
  ) {
    super(RemoteMethod, dataSource);
  }
}
