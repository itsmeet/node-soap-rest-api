import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'node_soap_rest_api',
  connector: 'memory',
  localStorage: '',
  file: './data/db.json'
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class NodeSoapRestApiDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'node_soap_rest_api';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.node_soap_rest_api', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
