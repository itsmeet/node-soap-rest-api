import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class RemoteMethod extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  name?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<RemoteMethod>) {
    super(data);
  }
}

export interface RemoteMethodRelations {
  // describe navigational properties here
}

export type RemoteMethodWithRelations = RemoteMethod & RemoteMethodRelations;
