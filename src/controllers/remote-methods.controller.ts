import {Count, CountSchema, Filter, FilterExcludingWhere, repository, Where} from '@loopback/repository';
import {del, get, getModelSchemaRef, HttpErrors, param, patch, post, put, requestBody} from '@loopback/rest';
import {toJson} from 'xml2json';
import {RemoteMethod} from '../models';
import {RemoteMethodRepository} from '../repositories';
export class RemoteMethodsController {
  constructor(
    @repository(RemoteMethodRepository)
    public remoteMethodRepository : RemoteMethodRepository,
  ) {}

  @post('/remote-methods', {
    responses: {
      '200': {
        description: 'RemoteMethod model instance',
        content: {'application/json': {schema: getModelSchemaRef(RemoteMethod)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RemoteMethod, {
            title: 'NewRemoteMethod',
          }),
        },
      },
    })
    remoteMethod: RemoteMethod,
  ): Promise<RemoteMethod> {
    return this.remoteMethodRepository.create(remoteMethod);
  }

  @get('/remote-methods/count', {
    responses: {
      '200': {
        description: 'RemoteMethod model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(RemoteMethod) where?: Where<RemoteMethod>,
  ): Promise<Count> {
    return this.remoteMethodRepository.count(where);
  }

  @get('/remote-methods', {
    responses: {
      '200': {
        description: 'Array of RemoteMethod model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(RemoteMethod, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(RemoteMethod) filter?: Filter<RemoteMethod>,
  ): Promise<RemoteMethod[]> {
    return this.remoteMethodRepository.find(filter);
  }

  @patch('/remote-methods', {
    responses: {
      '200': {
        description: 'RemoteMethod PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RemoteMethod, {partial: true}),
        },
      },
    })
    remoteMethod: RemoteMethod,
    @param.where(RemoteMethod) where?: Where<RemoteMethod>,
  ): Promise<Count> {
    return this.remoteMethodRepository.updateAll(remoteMethod, where);
  }

  @get('/remote-methods/{id}', {
    responses: {
      '200': {
        description: 'RemoteMethod model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(RemoteMethod, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(RemoteMethod, {exclude: 'where'}) filter?: FilterExcludingWhere<RemoteMethod>
  ): Promise<RemoteMethod> {
    return this.remoteMethodRepository.findById(id, filter);
  }

  @patch('/remote-methods/{id}', {
    responses: {
      '204': {
        description: 'RemoteMethod PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RemoteMethod, {partial: true}),
        },
      },
    })
    remoteMethod: RemoteMethod,
  ): Promise<void> {
    await this.remoteMethodRepository.updateById(id, remoteMethod);
  }

  @put('/remote-methods/{id}', {
    responses: {
      '204': {
        description: 'RemoteMethod PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() remoteMethod: RemoteMethod,
  ): Promise<void> {
    await this.remoteMethodRepository.replaceById(id, remoteMethod);
  }

  @del('/remote-methods/{id}', {
    responses: {
      '204': {
        description: 'RemoteMethod DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.remoteMethodRepository.deleteById(id);
  }

  // Map to `POST /add-remote-methods-from-wsdl`
  @post('/add-remote-methods-from-wsdl', {
    responses: {
      '200': {
        description: 'RemoteMethod model instance',
        content: {'application/json': {schema: getModelSchemaRef(RemoteMethod)}},
      },
    },
  })
  async createRemoteMethodFromWsdl(
    @requestBody({
      content: {
        'text/xml': {
          schema: {type: 'string'},
          example: `<?xml version="1.0" encoding="utf-8"?>
          <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
              <NumberToWords xmlns="http://www.dataaccess.com/webservicesserver/">
                <ubiNum>500</ubiNum>
              </NumberToWords>
            </soap:Body>
          </soap:Envelope>`
        },
      },
    })
    xmlInput: string,
  ): Promise<Array<Object>> {
    try {
      // Used module xml2json which converts large size of xml inputs efficiently
      const wsdlInput = JSON.parse(toJson(xmlInput.trim()));
      const methods: Array<Promise<RemoteMethod>> = [];

      // Collected method names dynamically and inserted them in remoteMethods
      Object.keys(wsdlInput).forEach((envelope) => {
        if (envelope.toLowerCase().includes("envelope"))
          Object.keys(wsdlInput[envelope]).forEach((body) => {
            if (body.toLowerCase().includes("body"))
              Object.keys(wsdlInput[envelope][body]).forEach((method) =>
                methods.push(this.remoteMethodRepository.create({name: method}))
              );
          });
      });

      return Promise.all(methods);
    } catch (error) {
      if (error.message.includes('not well-formed'))
        throw new HttpErrors.UnprocessableEntity(error.message)
      else
        throw new error
    }
  }
}
