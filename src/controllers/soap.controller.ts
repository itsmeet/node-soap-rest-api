import {inject} from '@loopback/context';
import {repository} from '@loopback/repository';
import {get, HttpErrors, Request, ResponseObject, RestBindings} from '@loopback/rest';
import {RemoteMethodRepository} from '../repositories';
/**
 * OpenAPI response for soap()
 */
const SOAP_RESPONSE: ResponseObject = {
  description: 'Soap Response',
  content: {
      'text/html': {
          schema: {type: 'string'}
      }
  }
};

/**
 * Controller to handle soap calls
 */
export class SoapController {
  constructor(
    @repository(RemoteMethodRepository)
    public remoteMethodRepository : RemoteMethodRepository,
    @inject(RestBindings.Http.REQUEST)
    private req: Request) {}

  // Map to `GET /{soapMethod}`
  @get('/{soapMethod}', {
    responses: {
      '200': SOAP_RESPONSE,
    },
    parameters:[{name: 'soapMethod', in: 'path', schema: {type: 'string'}}]
  })
  async soapMethod(soapMethod: string): Promise<string> {
    const isAvailable = await this.remoteMethodRepository
      .find({where: {and: [{'name': soapMethod.trim()}]}})
      .catch(error => {throw new Error(error)})

    // Ignoring test route for now as test cases are implemented using it
    if (soapMethod !== 'test' && isAvailable.length === 0) throw new HttpErrors.NotFound
    // Reply with a hello world if soap method is found
    return Promise.resolve('hello world');
  }
}
