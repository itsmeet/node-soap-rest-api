import {Client, expect} from '@loopback/testlab';
import {NodeSoapRestApiApplication} from '../..';
import {setupApplication} from './test-helper';

describe('SoapController', () => {
  let app: NodeSoapRestApiApplication;
  let client: Client;

  before('setupApplication', async () => {
    ({app, client} = await setupApplication());
  });

  after(async () => {
    await app.stop();
  });

  it('invokes GET /test', async () => {
    const res = await client.get('/test').expect(200);
    expect(res.text).equal('hello world');
  });
});
