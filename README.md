# node-soap-rest-api

[![LoopBack](<https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png>)](http://loopback.io/)

# Description

- This is a sample project which replicates SOAP api's with dynamic endpoint accessibility

# prerequisite

- Node version >=10

# Setup Instructions

- npm install
- npm start

# Endpoints

- APIDOC: http://localhost:3000/

# Insert remote methods using method name

- Open APIDOC URL http://localhost:3000/explorer/#/RemoteMethodsController/RemoteMethodsController.create
- Click on Try it out button on the left hand side of Post endpoint /remote-methods
- In request body replace your desired name with "string" in the "name" property
- Click Execute to register the method

# Insert remote methods using wsdl input

- Open APIDOC URL http://localhost:3000/explorer/#/RemoteMethodsController/RemoteMethodsController.createRemoteMethodFromWsdl
- Click on Try it out button on the left hand side of Post endpoint /add-remote-methods-from-wsdl
- There will be a sample wsdl which will create a method called "NumberToWords" in case it does not exists
- Click Execute to register the method

# Using soap endpoint

- Open APIDOC URL http://localhost:3000/explorer/#/SoapController/SoapController.soapMethod
- Click on Try it out button on the left hand side of Get endpoint /{soapMethod}
- In URL path parameter soapMethod enter your method name or use test keyword which is allowed permanently for unit tests.
- It will return hello world if method is registered or 404 not found if method is not registered.
- The SOAP endpoint can also be tested in the browser using URL endpoint http://localhost:3000/test
- All package.json scripts are working including unit test.
